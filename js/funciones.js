const contenedorArticulos = document.querySelector('#articles'),
      scrollArticulos = document.querySelector('#scrollArticulos'),
      btnAccion =  document.querySelector("#btnAccionArticulo"),
      tablaAsync = document.querySelector("#tablaAsync");

let articulos = [
    ['Celular','Samsung','5000','celular.png','Descripción celular'],
    ['Computadora','HP','12000','laptophp.png','Descripción Computadora'],
    ['Microfono','Sony','700','microfono.png','Descripción Microfono'],
    ['Lampara','Luz','1200','lampara.png','Descripción Lampara']
],
contadorBtnAccion = 0;

//Función para crear div y carta recibiendo el articulo.
const crearDivArticulo = (idArticulo, art,accion)=>{
    const articuloElement = document.createElement('div');
    let btnValue = '';
    let botonAccion = ``;

    if(accion === 1){
        btnValue = 'Agregar';
        botonAccion = `<button class="btn btn-primary" style="margin-left: 77px;" onclick="seleccionarArticulo(${idArticulo})"> ${btnValue}</button>`;
        articuloElement.classList.add('col-3');

    }else{
        btnValue = 'Eliminar';
        botonAccion = `<button class="btn btn-danger" style="margin-left: 77px;" onclick="regresarArticulo(${idArticulo})"> ${btnValue}</button>`;
        articuloElement.classList.add('art-scroll');

    }
    articuloElement.id = 'Articulo'+idArticulo;
    
    articuloElement.innerHTML = `
    <div class="card" style="width: 18rem">
        <img class="card-img-top imgArt" src="images/${ art[3] }" alt="Card image cap">
        <div class="card-body">
            <h5 class="card-title">${ art[0] } Marca: ${ art[1] }</h5>
            <p class="card-text">Precio: ${ art[2] } <br> ${ art[4] }</p>
            ${botonAccion}
        </div>
    </div>
    `;
    return articuloElement;
};

//Función para mostrar los articulos que existen.
const mostrarArticulos = ()=>{
    let ListaArticulos = '';
    const accion = 'Agregar';
    for ( let i = 0; i<articulos.length; i++){
        contenedorArticulos.append(crearDivArticulo(i,articulos[i],1));
    }

};

//Función para regresar un articulo que ha sido seleccionado a los que no han sido selecionados
const regresarArticulo = (idArticulo)=>{
    const accion = 'Agregar';
    const articuloEliminado = document.querySelector('#Articulo'+idArticulo);
    articuloEliminado.remove();
    contenedorArticulos.append(crearDivArticulo(idArticulo,articulos[idArticulo],1));
};
//Función para seleccionar un articulo
const seleccionarArticulo = (idArticulo)=>{

    const accion = 'Eliminar';
    const articuloEliminado = document.querySelector('#Articulo'+idArticulo);
    articuloEliminado.remove();
    scrollArticulos.append(crearDivArticulo(idArticulo,articulos[idArticulo],2));
};

mostrarArticulos();

//Acción al dar click al botón Nuevo articulo
document.querySelector("#btnAccionArticulo").onclick =  () =>{
    if(contadorBtnAccion%2 == 0){
        document.querySelector("#formCrearArticulo").style.display = "block";
        btnAccion.classList.remove('btn-primary');
        btnAccion.classList.add('btn-danger');
        btnAccion.value = 'cancelar';
    }else{
        document.querySelector("#formCrearArticulo").style.display = "none";
        btnAccion.classList.remove('btn-danger');
        btnAccion.classList.add('btn-primary');
        btnAccion.value = 'Crear Articulo';
    }
    contadorBtnAccion++;
    
}

//Acción al dar click al botón 
document.querySelector("#btnCrearArticulo").onclick =  () =>{ 
    const nombreArticulo = document.querySelector('#nombreArticulo'),
          marcaArticulo = document.querySelector('#marcaArticulo'),
          precioArticulo = document.querySelector('#precioArticulo'),
          descripcionArticulo = document.querySelector('#descripcionArticulo'),
          spanNombreArticulo = document.querySelector("#spanNombreArticulo"),
          spanMarcaArticulo = document.querySelector("#spanMarcaArticulo"),
          spanPrecioArticulo = document.querySelector("#spanPrecioArticulo"),
          spanDescripciónArticulo = document.querySelector("#spanDescripciónArticulo"),
          span = document.querySelectorAll('.spanArticulo'),
          inputDataArticulo = document.querySelectorAll('.inputDataArticulo');
          console.log(span);
    let validar = true;
    if(nombreArticulo.value === ""){
        nombreArticulo.style.borderColor = "red";
        spanNombreArticulo.innerHTML="Ingrese el nombre del articulo";
        spanNombreArticulo.style.display = "block";
        validar = false;
    }
    if(marcaArticulo.value === ""){
        marcaArticulo.style.borderColor = "red";
        spanMarcaArticulo.innerHTML="Ingrese la marca del articulo";
        spanMarcaArticulo.style.display = "block";
        validar = false;
    }
    if( precioArticulo.value === ""){
        precioArticulo.style.borderColor = "red";
        spanPrecioArticulo.innerHTML="Ingrese el precio del articulo";
        spanPrecioArticulo.style.display = "block";
        validar = false;
    }
    if(descripcionArticulo.value === ""){
        descripcionArticulo.style.borderColor = "red";
        spanDescripciónArticulo.innerHTML="Ingrese la descripción del articulo";
        spanDescripciónArticulo.style.display = "block";
        validar = false;
    }
    if(isNaN(precioArticulo.value)){
        precioArticulo.style.borderColor = "red";
        spanPrecioArticulo.innerHTML="Ingrese un precio del articulo valido";
        spanPrecioArticulo.style.display = "block";

        validar = false;
    }

    if(validar){
        try{
            const idArticulo = articulos.length;
            console.log(idArticulo);
            articulos.push(
                    [
                        nombreArticulo.value,
                        marcaArticulo.value,
                        precioArticulo.value,
                        'articulo.jpg',
                        descripcionArticulo.value
                    ]
                );
            console.log(articulos);
            contenedorArticulos.append(crearDivArticulo(idArticulo,articulos[idArticulo],1));
            span.forEach(el => el.style.display = "none");
            inputDataArticulo.forEach(el => el.style.borderColor = "#ced4da");
        }catch(e){

        }
          
    }else{
        console.log("Ha ocurrido un error al crear el articulo");
    } 
};

//Función Asyncrona para mostrar usuarios del archivo data/data.json
async function loadTeam() {
    const response = await fetch("data/data.json");
    const team = await response.json();
    
    return team;
}
//Función asyncrona para obtener datos y añadirlos a una tabla.
document.addEventListener("DOMContentLoaded", async () =>{
    let team = [];
    try{
        team = await loadTeam();
        // Crea un elemento <table> y un elemento <tbody>
        const tabla   = document.createElement("table");
        tabla.classList.add('table');
        const tblBody = document.createElement("tbody");
        console.log(team);
        console.log(team[0]);
        const hileraH = document.createElement("tr");
        const thId = document.createElement("th");
        const thName = document.createElement("th");
        const tituloCeldaId = document.createTextNode("Id");
        const tituloCeldaName = document.createTextNode("Name");
        thId.appendChild(tituloCeldaId);
        thName.appendChild(tituloCeldaName);

        hileraH.appendChild(thId);
        hileraH.appendChild(thName);
        
        tblBody.appendChild(hileraH);
        // Crea las celdas
        for (let i = 0; i < team.length; i++) {
            // Crea las hileras de la tabla
            const hilera = document.createElement("tr");

            // Crea un elemento <td> y un nodo de texto, haz que el nodo de
            // texto sea el contenido de <td>, ubica el elemento <td> al final
            // de la hilera de la tabla
            const celdaId = document.createElement("td");
            const textoCeldaId = document.createTextNode(i);
            const celdaName = document.createElement("td");
            const textoCelda = document.createTextNode(team[i]['name']);
            celdaId.appendChild(textoCeldaId);
            hilera.appendChild(celdaId);
            celdaName.appendChild(textoCelda);
            hilera.appendChild(celdaName);
            

            // agrega la hilera al final de la tabla (al final del elemento tblbody)
            tblBody.appendChild(hilera);
        }

        // posiciona el <tbody> debajo del elemento <table>
        tabla.appendChild(tblBody);
        // appends <table> into <body>
        tablaAsync.appendChild(tabla);
        // modifica el atributo "border" de la tabla y lo fija a "2";
        tabla.setAttribute("border", "2");
        
    }catch(e){
        console.log("Error");
        console.log(e);
    } 
    
});
